﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkCore.Models
{
    public class StudentSchool
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int Age { get; set; }
        public int SchoolId { get; set; }
        public int IdSc { get; set; }
        public string NomSc { get; set; }
    }
}
