﻿using System;
using System.Collections.Generic;

namespace DAL.DAL
{
    public partial class StudentCours
    {
        public int Id { get; set; }
        public int CoursId { get; set; }
        public int StudentId { get; set; }

        public Cours Cours { get; set; }
        public Students Student { get; set; }
    }
}
