﻿using System;
using System.Collections.Generic;

namespace DAL.DAL
{
    public partial class Cours
    {
        public Cours()
        {
            StudentCours = new HashSet<StudentCours>();
        }

        public int Id { get; set; }
        public string Nom { get; set; }

        public ICollection<StudentCours> StudentCours { get; set; }
    }
}
