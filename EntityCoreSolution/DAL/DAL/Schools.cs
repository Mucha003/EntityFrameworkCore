﻿using System;
using System.Collections.Generic;

namespace DAL.DAL
{
    public partial class Schools
    {
        public Schools()
        {
            Students = new HashSet<Students>();
        }

        public int Id { get; set; }
        public string Nom { get; set; }

        public ICollection<Students> Students { get; set; }
    }
}
