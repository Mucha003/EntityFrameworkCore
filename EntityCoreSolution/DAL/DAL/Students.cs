﻿using System;
using System.Collections.Generic;

namespace DAL.DAL
{
    public partial class Students
    {
        public Students()
        {
            StudentCours = new HashSet<StudentCours>();
        }

        public int Id { get; set; }
        public string Nom { get; set; }
        public int Age { get; set; }
        public int SchoolId { get; set; }

        public Schools School { get; set; }
        public Adresses Adresses { get; set; }
        public ICollection<StudentCours> StudentCours { get; set; }
    }
}
