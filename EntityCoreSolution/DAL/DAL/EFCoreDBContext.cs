﻿using Microsoft.EntityFrameworkCore;

namespace DAL.DAL
{
    public partial class EFCoreDBContext : DbContext
    {
        public EFCoreDBContext()
        {
        }

        public EFCoreDBContext(DbContextOptions<EFCoreDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Adresses> Adresses { get; set; }
        public virtual DbSet<Cours> Cours { get; set; }
        public virtual DbSet<Schools> Schools { get; set; }
        public virtual DbSet<StudentCours> StudentCours { get; set; }
        public virtual DbSet<Students> Students { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=.; Initial Catalog=EFCoreDB; User ID=sa; Password=rootroot;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Adresses>(entity =>
            {
                entity.HasIndex(e => e.StudentId)
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.StudentId).HasColumnName("StudentID");

                entity.HasOne(d => d.Student)
                    .WithOne(p => p.Adresses)
                    .HasForeignKey<Adresses>(d => d.StudentId);
            });

            modelBuilder.Entity<Cours>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");
            });

            modelBuilder.Entity<Schools>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");
            });

            modelBuilder.Entity<StudentCours>(entity =>
            {
                entity.HasKey(e => new { e.CoursId, e.StudentId });

                entity.HasIndex(e => e.StudentId);

                entity.Property(e => e.CoursId).HasColumnName("CoursID");

                entity.Property(e => e.StudentId).HasColumnName("StudentID");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.HasOne(d => d.Cours)
                    .WithMany(p => p.StudentCours)
                    .HasForeignKey(d => d.CoursId);

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentCours)
                    .HasForeignKey(d => d.StudentId);
            });

            modelBuilder.Entity<Students>(entity =>
            {
                entity.HasIndex(e => e.SchoolId);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.SchoolId).HasColumnName("SchoolID");

                entity.HasOne(d => d.School)
                    .WithMany(p => p.Students)
                    .HasForeignKey(d => d.SchoolId);
            });
        }
    }
}
