﻿using System;
using System.Collections.Generic;

namespace DAL.DAL
{
    public partial class Adresses
    {
        public int Id { get; set; }
        public string Rue { get; set; }
        public int StudentId { get; set; }

        public Students Student { get; set; }
    }
}
