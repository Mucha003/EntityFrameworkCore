﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class StudentSchool
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int Age { get; set; }
        public int SchoolId { get; set; }
        public int Idsc { get; set; }
        public string Nomsc { get; set; }
    }
}
