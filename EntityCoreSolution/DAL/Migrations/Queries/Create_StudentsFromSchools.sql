﻿Create Procedure [dbo].[_sp_StudentsFromSchools]
AS
BEGIN
	SET NOCOUNT ON;
	select s.ID, s.Nom, s.Age, s.SchoolID
	from Students s
	inner join Schools sc on s.SchoolID  = sc.ID
END