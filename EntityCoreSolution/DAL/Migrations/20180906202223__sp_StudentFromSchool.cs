﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class _sp_StudentFromSchool : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = Sql.RessourcesSQL.Create_StudentsFromSchools;
            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
