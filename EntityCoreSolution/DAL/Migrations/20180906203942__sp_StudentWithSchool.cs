﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class _sp_StudentWithSchool : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = Sql.RessourcesSQL._sp_StudentWithSchool;
            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
